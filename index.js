// This is used to get the contents of the express package to be used by the application
const express = require("express");

// Creates an application using express
// This creates an express application and stores it in a variable
// In layman's term, app is our server
const app = express();

// Four our application server to run, we need a port to listen to
const port = 3000;

// Setup for allowing the server to handle data from requests
// Allows our app to read JSON data
// Middleware is a request handler that has access to the application's request and response cycle
app.use(express.json())

// Mock database
let users = [
	{
		username: "TStark3000",
		email: "starkindustries@mail.com",
		password: "notPeterParker"
	},
	{
		username: "ThorThunder",
		email: "loveAndThunder@mail.com",
		password: "iLoveStormBreaker"
	}
];



// [SECTION] Routes

	// Express has methods corresponding to each http method
	
	// This route expects to receive a GET request at the base URI
	// This will return a simple message back to the client
	app.get("/", (req, res) => {

		// Combines writeHead() and end()
		// it used the express JS moduless method instead to send a response back to the client
		res.send("Hello World")
	})

	/*
		Mini Activity: 5 mins

			>> Create a get route in Expressjs which will be able to send a message in the client:

				>> endpoint: /greeting
				>> message: 'Hello from Batch244-surname'

			>> Test in postman
			>> Send your response in hangouts
	*/
	// SOLUTION
	app.get("/greeting", (req, res) => {
		res.send("Hello from batch244-surname")
	})

	// This route expects to receivie a POST request

	app.post("/hello", (req, res) => {
		res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
	})


	// This route expects to receive a post request at the URI "/signup"
	app.post("/signup", (req, res) =>{

		if(req.body.username !== "" && req.body.password !== "" && req.body.email !== ""){
			users.push(req.body)
			res.send(`user ${req.body.username} is successfully registered!`)
		} else {
			res.send("please input BOTH username and password")
		}
	})

	// Retrieving all users
	app.get("/users", (req, res) =>{
		res.send(users);
	})

	// This route expects to receive a PUT request
	// This will update the password of a user that matches the information provided in the client/Postman
	app.put("/change-password", (req, res) => {
		let message;

		for(let i = 0; i < users.length; i++){

			if (req.body.username == users[i].username){
				users[i].password = req.body.password

				message = `User ${req.body.username}'s password has been updated`
				break;
			} else {
				message = "User does not exist."
			}
		}
		res.send(message);
	})

	app.get("/home", (req, res) => {

		res.send("Welcome to the home page")
	})

	// app.delete("/delete-user", (req, res) => {
	// 	res.send(`Username ${req.body.username} has been deleted`)
	// })

	app.delete("/delete-user", (req, res) => {
		let message;

		for(let i = 0; i < users.length; i++){

			if (req.body.username == users[i].username){
				users.splice(users.indexOf(req.body))

				message = `User ${req.body.username} has been deleted`
				break;
			} else {
				message = "User does not exist."
			}
		}
		res.send(message)
	})


app.listen(port, () => console.log(`Server is running at port ${port}`))